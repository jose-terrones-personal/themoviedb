package com.app.themoviedb.interfaces

import com.app.themoviedb.data.models.MovieListResponse
import com.app.themoviedb.data.models.ResultsItem

interface MovieListActions {
    fun onSuccess(t: MovieListResponse)
    fun onError(r: Throwable)
}

interface OnMovieListItemClickListener {
    fun onMovieListItemClicked(t: ResultsItem?)
}