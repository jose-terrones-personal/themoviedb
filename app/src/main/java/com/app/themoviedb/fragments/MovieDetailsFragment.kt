package com.app.themoviedb.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.myapplication.R
import com.app.themoviedb.data.models.ResultsItem
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_movie_details.*

class MovieDetailsFragment : BaseFragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater.inflate(R.layout.fragment_movie_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        arguments?.let {
            val stringData = it.getString("data")
            val data = Gson().fromJson(stringData, ResultsItem::class.java)

            data?.let { movie ->
                addMovieDetails(movie)
            }
        }
    }

    private fun addMovieDetails(movie: ResultsItem) {
        movieTitle.text = movie.title
        movieRating.text = movie.voteAverage.toString()
        moviewDate.text = movie.releaseDate
        movieDescription.text = movie.overview
    }

}