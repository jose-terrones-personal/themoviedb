package com.app.themoviedb.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.app.myapplication.R

abstract class BaseActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutTagId())
    }

    protected abstract fun getLayoutTagId(): Int

    fun addFragment(fragment: Fragment, isNull: Boolean) {
        if (isNull) {
            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(R.id.fragmentLayout, fragment)
            transaction.addToBackStack(fragment.tag)
            transaction.commit()
        }
    }
}