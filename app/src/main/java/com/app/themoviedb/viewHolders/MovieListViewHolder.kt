package com.app.themoviedb.viewHolders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.app.themoviedb.data.models.ResultsItem
import com.app.themoviedb.interfaces.OnMovieListItemClickListener
import kotlinx.android.synthetic.main.movie_list_item_thumbnail.view.*

class MovieListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun setMovieListItemClickListiner(data: ResultsItem?, clickListener: OnMovieListItemClickListener) {
        itemView.gridImageItem.setOnClickListener {
            clickListener.onMovieListItemClicked(data)
        }
    }

}