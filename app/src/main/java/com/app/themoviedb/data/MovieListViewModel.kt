package com.app.themoviedb.data

import androidx.lifecycle.ViewModel
import com.app.themoviedb.client.GetData
import com.app.themoviedb.data.models.MovieListResponse
import com.app.themoviedb.interfaces.MovieListActions
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

// https://api.themoviedb.org/3/discover/movie?api_key=fe9eba23db2c20f0b5795db053f29685

class MovieListViewModel : ViewModel() {
    private fun buildRetrofit(): GetData {
        return Retrofit.Builder()
            .baseUrl("https://api.themoviedb.org/3/")
            // Specify the converter factory to use for serialization and deserialization
            .addConverterFactory(GsonConverterFactory.create())
            // Add a call adapter factory to support RxJava return types
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            // Build the Retrofit instance
            .build().create(GetData::class.java)
    }

    fun getData(response: MovieListActions) {
        buildRetrofit()
            .getMovieList()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ t: MovieListResponse -> response.onSuccess(t) },
                { r: Throwable -> response.onError(r) })
    }
}