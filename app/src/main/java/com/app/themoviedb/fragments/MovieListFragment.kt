package com.app.themoviedb.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.myapplication.R
import com.app.themoviedb.adapters.MovieListAdapter
import com.app.themoviedb.data.MovieListViewModel
import com.app.themoviedb.data.models.MovieListResponse
import com.app.themoviedb.data.models.ResultsItem
import com.app.themoviedb.interfaces.MovieListActions
import com.app.themoviedb.interfaces.OnMovieListItemClickListener
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_home.*

class MovieListFragment : BaseFragment(), MovieListActions, OnMovieListItemClickListener {
    private lateinit var viewModel: MovieListViewModel
    private lateinit var recycleView: RecyclerView
    private lateinit var viewAdapter: MovieListAdapter
    private lateinit var viewManager: RecyclerView.LayoutManager

    override fun onSuccess(t: MovieListResponse) = viewAdapter.setData(t)

    override fun onError(r: Throwable) = print("ERROR:: ${r.localizedMessage}")

    override fun onMovieListItemClicked(t: ResultsItem?) {
        val frag = MovieDetailsFragment()
        val bundle = Bundle()
        val stringData = Gson().toJson(t)
        bundle.putString("data", stringData)

        if(!bundle.isEmpty) frag.arguments = bundle
        addFragment(frag, R.id.fragmentLayout)
    }

//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        viewModel = ViewModelProviders.of(requireActivity()).get(MovieListViewModel::class.java)
//        viewModel.getData(this)
//    }

    // TODO: onCreate only happens once, back button wont load the data (cache)
    override fun onResume() {
        super.onResume()
        viewModel = ViewModelProviders.of(requireActivity()).get(MovieListViewModel::class.java)
        viewModel.getData(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewManager = GridLayoutManager(context, 3)
        viewAdapter = MovieListAdapter(this)

        recycleView = moviewListRecyclerView?.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
        }!!
    }

}