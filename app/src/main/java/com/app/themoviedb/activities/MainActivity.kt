package com.app.themoviedb.activities

import android.os.Bundle
import com.app.myapplication.R
import com.app.themoviedb.fragments.MovieListFragment

class MainActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        addFragment(MovieListFragment(), savedInstanceState == null)
    }

    override fun getLayoutTagId(): Int = R.layout.activity_main

}
