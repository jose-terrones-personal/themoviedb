package com.app.themoviedb.fragments

import androidx.fragment.app.Fragment

abstract class BaseFragment : Fragment() {
    fun addFragment(fragment: Fragment, layoutTagId: Int) {
        activity?.supportFragmentManager?.beginTransaction()?.apply {
            replace(layoutTagId, fragment)
            addToBackStack(fragment.tag)
            commit()
        }
    }

}