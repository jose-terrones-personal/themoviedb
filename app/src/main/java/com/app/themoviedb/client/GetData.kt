package com.app.themoviedb.client

import com.app.themoviedb.data.models.MovieListResponse
import io.reactivex.Observable
import retrofit2.http.GET

interface GetData {
    @GET("discover/movie?api_key=fe9eba23db2c20f0b5795db053f29685")
    fun getMovieList(): Observable<MovieListResponse>
}