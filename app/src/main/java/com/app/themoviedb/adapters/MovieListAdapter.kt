package com.app.themoviedb.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.myapplication.R
import com.app.themoviedb.data.models.MovieListResponse
import com.app.themoviedb.interfaces.OnMovieListItemClickListener
import com.app.themoviedb.viewHolders.MovieListViewHolder

class MovieListAdapter(val movieListItemClickListiner: OnMovieListItemClickListener) : RecyclerView.Adapter<MovieListViewHolder>() {
    lateinit var movieList: MovieListResponse

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieListViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.movie_list_item_thumbnail, parent, false)
        return MovieListViewHolder(view)
    }

    override fun getItemCount(): Int {
        if (::movieList.isInitialized) {
            return movieList.results?.size ?: 0
        }
        return 0
    }

    override fun onBindViewHolder(holder: MovieListViewHolder, position: Int) {
        if (::movieList.isInitialized) {
            holder.setMovieListItemClickListiner(movieList?.results?.get(position), movieListItemClickListiner)
        }
    }

    fun setData(t: MovieListResponse) {
        movieList = t
        notifyDataSetChanged()
    }
}